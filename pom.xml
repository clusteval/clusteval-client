<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<artifactId>clusteval-client</artifactId>

	<properties>
		<timestamp>${maven.build.timestamp}</timestamp>
		<maven.build.timestamp.format>dd-MM-yyyy HH:mm</maven.build.timestamp.format>
	</properties>


	<repositories>
		<repository>
                        <id>compbio_maven</id>
			<releases>
				<enabled>true</enabled>
                        </releases>
                        <name>Internal Release Repository</name>
                        <url>http://maven.compbio.sdu.dk/repository/internal/</url>
                </repository>
		<repository>
			<id>archiva.snapshots</id>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
			<releases>
			</releases>
			<name>CompBio Maven Repository</name>
			<url>http://maven.compbio.sdu.dk/repository/snapshots/</url>
		</repository>
		<repository>
			<id>central_maven</id>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
			<releases>
			</releases>
			<name>Central Maven</name>
			<url>http://central.maven.org/</url>
		</repository>
	</repositories>

	<distributionManagement>
		<repository>
			<id>compbio_maven</id>
			<name>Internal Release Repository</name>
			<url>http://maven.compbio.sdu.dk/repository/internal/</url>
		</repository>
		<snapshotRepository>
			<uniqueVersion>true</uniqueVersion>
			<id>archiva.snapshots</id>
			<name>Snapshots</name>
			<url>http://maven.compbio.sdu.dk/repository/snapshots/</url>
			<layout>default</layout>
		</snapshotRepository>
	</distributionManagement>


	<dependencies>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.11</version>
			<scope>test</scope>
		</dependency>



		<dependency>
			<groupId>commons-cli</groupId>
			<artifactId>commons-cli</artifactId>
			<version>1.4</version>
		</dependency>

		<dependency>
			<groupId>commons-configuration</groupId>
			<artifactId>commons-configuration</artifactId>
			<version>1.10</version>
		</dependency>




		<dependency>
			<groupId>jline</groupId>
			<artifactId>jline</artifactId>
			<version>2.8</version>
		</dependency>

		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.4</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>de.clusteval</groupId>
			<artifactId>clusteval-lib</artifactId>
			<version>1.8.1</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>de.clusteval</groupId>
			<artifactId>clusteval-server</artifactId>
			<version>1.8.1</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>de.clusteval</groupId>
			<artifactId>clusteval-client-lib</artifactId>
			<version>1.8.1</version>
		</dependency>
		<dependency>
			<groupId>dk.sdu.imada.compbio</groupId>
			<artifactId>utils</artifactId>
			<version>1.0.0</version>
		</dependency>
	</dependencies>

	<build>
		<sourceDirectory>src</sourceDirectory>
		<testSourceDirectory>test</testSourceDirectory>

		<resources>
			<resource>
				<directory>${basedir}/src/</directory>
				<filtering>true</filtering>
				<includes>
					<include>*.date</include>
				</includes>
			</resource>
		</resources>

		<plugins>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.6.1</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>com.google.code.maven-replacer-plugin</groupId>
				<artifactId>replacer</artifactId>
				<version>1.5.3</version>
				<executions>
					<execution>
						<phase>process-sources</phase>
						<goals>
							<goal>replace</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<basedir>${basedir}</basedir>
					<includes>
						<include>**/src/**/*.java</include>
					</includes>
					<regex>true</regex>
					<replacements>
						<replacement>
							<token>ClustEvalVersion\(version = [^\)]+</token>
							<value>ClustEvalVersion\(version = "${project.version}"</value>
						</replacement>
					</replacements>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<configuration>
					<additionalparam>-Xdoclint:none</additionalparam>
				</configuration>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<configuration>
					<archive>
						<manifest>
							<mainClass>de.clusteval.serverclient.ClustEvalCommandLineClient</mainClass>
						</manifest>
					</archive>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
					<appendAssemblyId>false</appendAssemblyId>
				</configuration>
				<executions>
					<execution>
						<id>make-assembly</id> <!-- this is used for inheritance merges -->
						<phase>package</phase> <!-- bind to the packaging phase -->
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	<scm>
		<developerConnection>scm:git:http://gitlab.compbio.sdu.dk/wiwiec/clustevalBackendClient.git</developerConnection>
		<tag>master</tag>
		<connection>scm:git:http://gitlab.compbio.sdu.dk/wiwiec/clustevalBackendClient.git</connection>
		<url>http://gitlab.compbio.sdu.dk/wiwiec/clustevalBackendClient.git</url>
	</scm>
	<version>1.8.1</version>
	<groupId>de.clusteval</groupId>
</project>
