/**
 * 
 */
package de.clusteval.serverclient;

import de.clusteval.utils.ClustEvalException;

/**
 * @author Christian Wiwie
 *
 */
public class ParameterParseException extends ClustEvalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -770881151124313037L;

	/**
	 * @param message
	 * @param cause
	 */
	public ParameterParseException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ParameterParseException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ParameterParseException(Throwable cause) {
		super(cause);
	}

}
