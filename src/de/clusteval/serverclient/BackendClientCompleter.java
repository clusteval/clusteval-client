/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.serverclient;

import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.rmi.UnknownHostException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;

import de.clusteval.auth.OperationNotPermittedException;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.data.dataset.generator.IDataSetGenerator;
import de.clusteval.data.randomizer.IDataRandomizer;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.run.ISerializableRun;
import de.clusteval.run.RUN_STATUS;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import dk.sdu.imada.compbio.utils.ArraysExt;
import dk.sdu.imada.compbio.utils.Pair;
import jline.console.completer.Completer;

/**
 * This class is used by the backend client as a tab completer for the command
 * line.
 * 
 * <p>
 * Objects of this class communicate directly with the backend server to receive
 * information, that then in turn are shown in the command line as tab
 * completion candidates.
 * 
 * @author Christian Wiwie
 * 
 */
public class BackendClientCompleter implements Completer {

	/**
	 * The backend client that uses this completer.
	 */
	protected ClustEvalCommandLineClient client;

	/**
	 * The id of the client needed for communication between this completer and
	 * the server.
	 */
	protected String clientId;

	/**
	 * The parameters of the client (e.g. server ip and port) for communication
	 * between this completer and the server.
	 */
	protected String[] newArgs;

	/**
	 * A temporary variable holding the runs after they were retrieved from the
	 * server.
	 */
	protected TreeSet<String> runs;

	/**
	 * A temporary variable holding the dataset generators after they were
	 * retrieved from the server.
	 */
	protected TreeSet<String> dataSetGenerators;

	/**
	 * A temporary variable holding the data randomizers after they were
	 * retrieved from the server.
	 */
	protected TreeSet<String> dataRandomizers;

	/**
	 * A temporary variable holding the run resumes after they were retrieved
	 * from the server.
	 */
	protected TreeSet<String> runResumes;

	/**
	 * A temporary variable holding the run results after they were retrieved
	 * from the server.
	 */
	protected TreeSet<String> runResults;

	/**
	 * A temporary variable holding the run status after they were retrieved
	 * from the server.
	 */
	protected Map<String, Pair<RUN_STATUS, Float>> runStatus;

	/**
	 * A temporary variable holding the active runs after they were retrieved
	 * from the server.
	 */
	protected TreeSet<String> runningRuns;

	/**
	 * @param clientId
	 *            The id of the client needed for communication between this
	 *            completer and the server.
	 * @param args
	 *            The parameters of the client (e.g. server ip and port) for
	 *            communication between this completer and the server.
	 * @throws IncompatibleClustEvalVersionException
	 */
	public BackendClientCompleter(final String clientId, final String[] args)
			throws IncompatibleClustEvalVersionException {
		super();

		this.clientId = clientId;
		try {
			newArgs = ArraysExt.merge(args,
					new String[]{"-clientId", clientId});
			client = new ClustEvalCommandLineClient(newArgs);
		} catch (ConnectException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method updates the {@link #runs} attribute by retrieving the
	 * available runs from the server.
	 * 
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	protected void updateRuns()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		runs = new TreeSet<String>();

		Collection<ISerializableRun> runs = client.getRuns();
		for (ISerializableRun r : runs)
			this.runs.add(String.format("%s:%s", r.getName(), r.getVersion()));
	}

	/**
	 * This method updates the {@link #dataSetGenerators} attribute by
	 * retrieving the availble dataset generators from the server.
	 * 
	 * @throws RemoteException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	protected void updateDataSetGenerators() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		dataSetGenerators = new TreeSet<String>();
		for (ISerializableWrapperRepositoryObject<? extends IDataSetGenerator> g : client
				.getDataSetGenerators())
			dataSetGenerators.add(g.toString());
	}

	/**
	 * This method updates the {@link #runResumes} attribute by retrieving the
	 * available run resumes from the server.
	 * 
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	protected void updateRunResumes() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		runResumes = new TreeSet<String>(client.getRunResumes());
	}

	/**
	 * This method updates the {@link #runResults} attribute by retrieving the
	 * available run results from the server.
	 * 
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	protected void updateRunResults() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		runResults = new TreeSet<String>(
				client.getParameterOptimizationRunResultIdentifiers());
	}

	/**
	 * This method updates the {@link #runningRuns} attribute by retrieving the
	 * currently executed runs from the server.
	 */
	protected void updateRunningRuns() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		runStatus = client.getMyRunStatus();
		runningRuns = new TreeSet<String>();
		for (String run : runStatus.keySet())
			if (runStatus.get(run).getFirst().equals(RUN_STATUS.RUNNING)
					|| runStatus.get(run).getFirst()
							.equals(RUN_STATUS.SCHEDULED))
				runningRuns.add(run);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jline.console.completer.Completer#complete(java.lang.String, int,
	 * java.util.List)
	 */
	@SuppressWarnings("unused")
	@Override
	public int complete(String buffer, int cursor,
			List<CharSequence> candidates) {
		SortedSet<String> strings = new TreeSet<String>();

		Set<String> excludeCmds = new HashSet<String>();
		excludeCmds.add("ip");
		excludeCmds.add("port");
		excludeCmds.add("logLevel");
		excludeCmds.add("clientId");

		for (Object obj : ClustEvalCommandLineClient.clientCLIOptions.getOptions()) {
			Option o = (Option) obj;
			if (!(excludeCmds.contains(o.getOpt())))
				strings.add(o.getOpt());
		}

		boolean exception = true;
		while (exception) {
			exception = false;
			try {
				if (buffer == null) {
					candidates.addAll(strings);
				} else if (buffer.equals("performRun ")) {
					this.updateRuns();
					candidates.addAll(runs);
					return buffer.length();
				} else if (buffer.equals("resumeRun ")) {
					this.updateRunResumes();
					candidates.addAll(runResumes);
					return buffer.length();
				} else if (buffer.equals("getRunResults ")) {
					this.updateRunResults();
					candidates.addAll(runResults);
					return buffer.length();
				} else if (buffer.equals("getRunResumes ")) {
					this.updateRunResumes();
					candidates.addAll(runResumes);
					return buffer.length();
				} else if (buffer.equals("terminateRun ")
						|| buffer.equals("getRunStatus ")
						|| buffer.equals("getOptRunStatus ")) {
					this.updateRunningRuns();
					candidates.addAll(runningRuns);
					return buffer.length();
				} else if (buffer.startsWith("performRun ")) {
					updateRuns();
					int posSpace = buffer.indexOf(' ');
					for (String match : runs
							.tailSet(buffer.substring(posSpace + 1))) {
						if (!match.startsWith(buffer.substring(posSpace + 1))) {
							break;
						}

						candidates.add(match);
					}

					return posSpace + 1;
				} else if (buffer.startsWith("resumeRun ")) {
					this.updateRunResumes();
					int posSpace = buffer.indexOf(' ');
					for (String match : runResumes
							.tailSet(buffer.substring(posSpace + 1))) {
						if (!match.startsWith(buffer.substring(posSpace + 1))) {
							break;
						}

						candidates.add(match);
					}

					return posSpace + 1;
				} else if (buffer.startsWith("getRunResults ")) {
					this.updateRunResults();
					int posSpace = buffer.indexOf(' ');
					for (String match : runResults
							.tailSet(buffer.substring(posSpace + 1))) {
						if (!match.startsWith(buffer.substring(posSpace + 1))) {
							break;
						}

						candidates.add(match);
					}

					return posSpace + 1;
				} else if (buffer.startsWith("getRunResumes ")) {
					this.updateRunResumes();
					int posSpace = buffer.indexOf(' ');
					for (String match : runResumes
							.tailSet(buffer.substring(posSpace + 1))) {
						if (!match.startsWith(buffer.substring(posSpace + 1))) {
							break;
						}

						candidates.add(match);
					}

					return posSpace + 1;
				} else if (buffer.startsWith("generateDataSet ")) {
					this.updateDataSetGenerators();
					int posSpace = buffer.indexOf(' ');
					for (String match : dataSetGenerators
							.tailSet(buffer.substring(posSpace + 1))) {
						if (!match.startsWith(buffer.substring(posSpace + 1))) {
							break;
						}

						candidates.add(match);
					}

					return posSpace + 1;
				} else if (buffer.startsWith("randomizeDataConfig ")) {
					this.updateDataRandomizers();
					int posSpace = buffer.indexOf(' ');
					for (String match : dataRandomizers
							.tailSet(buffer.substring(posSpace + 1))) {
						if (!match.startsWith(buffer.substring(posSpace + 1))) {
							break;
						}

						candidates.add(match);
					}

					return posSpace + 1;
				} else if (buffer.startsWith("generateDataSet ")) {
					this.updateDataSetGenerators();
					candidates.addAll(dataSetGenerators);
					return buffer.length();
				} else if (buffer.startsWith("terminateRun ")
						|| buffer.startsWith("getRunStatus ")) {
					this.updateRunningRuns();
					int posSpace = buffer.indexOf(' ');
					for (String match : runningRuns
							.tailSet(buffer.substring(posSpace + 1))) {
						if (!match.startsWith(buffer.substring(posSpace + 1))) {
							break;
						}

						candidates.add(match);
					}

					return posSpace + 1;
				} else {
					for (String match : strings.tailSet(buffer)) {
						if (!match.startsWith(buffer)) {
							break;
						}

						candidates.add(match);
					}
				}
			} catch (RemoteException | OperationNotPermittedException
					| UnknownClientException | RepositoryObjectSerializationException e) {
				exception = true;
				try {
					// client = new EvalClient(new String[]{"-clientId",
					// clientId});
					client = new ClustEvalCommandLineClient(newArgs);
				} catch (ConnectException e1) {
				} catch (ParseException e1) {
				} catch (UnknownHostException e1) {
				} catch (IncompatibleClustEvalVersionException e1) {
				}
				// return -1;
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}

			if (candidates.size() == 1) {
				candidates.set(0, candidates.get(0) + " ");
			}
		}

		return candidates.isEmpty() ? -1 : 0;
	}

	/**
	 * This method updates the {@link #dataRandomizers} attribute by retrieving
	 * the availble data randomizers from the server.
	 * 
	 * @throws RemoteException
	 */
	protected void updateDataRandomizers() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		dataRandomizers = new TreeSet<String>();
		for (ISerializableWrapperRepositoryObject<? extends IDataRandomizer> g : client
				.getDataRandomizers())
			dataRandomizers.add(g.toString());
	}
}
