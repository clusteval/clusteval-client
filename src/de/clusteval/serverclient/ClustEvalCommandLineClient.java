/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.serverclient;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.ConnectException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.lang.WordUtils;
import org.apache.maven.artifact.versioning.ComparableVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import de.clusteval.auth.IClientPermissions;
import de.clusteval.auth.OperationNotPermittedException;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.generator.UnknownDataSetGeneratorException;
import de.clusteval.data.randomizer.DataRandomizeException;
import de.clusteval.data.randomizer.UnknownDataRandomizerException;
import de.clusteval.framework.repository.ObjectNotFoundException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotFoundException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.maven.InvalidDynamicComponentNameException;
import de.clusteval.program.r.UnknownRProgramException;
import de.clusteval.run.RUN_STATUS;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import dk.sdu.imada.compbio.format.Formatter;
import dk.sdu.imada.compbio.utils.ArraysExt;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.StringExt;
import dk.sdu.imada.compbio.utils.StringExt.ELLIPSIS_LOCATION;
import jline.console.ConsoleReader;
import jline.console.completer.Completer;

/**
 * A backend client can give commands to the backend server (see
 * {@link IBackendServer}). TODO: This class needs some serious refactoring.
 * Currently, the client is a thread. However, this is only used when executing
 * commands that wait for a run to finish. Can we remove this completely?
 * 
 * @author Christian Wiwie
 * 
 */
public class ClustEvalCommandLineClient extends ClustEvalClient
		implements
			Runnable {

	protected static String VERSION_INFO_STRING;

	/**
	 * This variable holds the command line options of the backend server.
	 */
	public static Options clientCLIOptions = new Options();

	static {
		// read properties file with version number
		Properties prop = new Properties();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream stream = loader.getResourceAsStream("client.date");
		try {
			prop.load(stream);
			VERSION_INFO_STRING = "Jar built: " + prop.getProperty("build.date")
					+ "\nVersion: " + getClientVersion();
		} catch (IOException e) {
			e.printStackTrace();
		}

		OptionBuilder.withArgName("level");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"The verbosity this client should use during its execution. 0=ALL, 1=TRACE, 2=DEBUG, 3=INFO, 4=WARN, 5=ERROR, 6=OFF");
		OptionBuilder.withType(Integer.class);
		Option option = OptionBuilder.create("logLevel");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription("Print this help and usage information");
		Option optionHelp = OptionBuilder.create("help");
		clientCLIOptions.addOption(optionHelp);

		OptionBuilder.withDescription("Print the version of this client");
		Option optionVersion = OptionBuilder.create("version");
		clientCLIOptions.addOption(optionVersion);

		OptionBuilder.withDescription(
				"Check for available updates for the ClustEval server");
		Option optionUpdate = OptionBuilder.create("checkForServerUpdate");
		clientCLIOptions.addOption(optionUpdate);

		OptionBuilder.withDescription(
				"Check for available updates for the ClustEval client");
		optionUpdate = OptionBuilder.create("checkForClientUpdate");
		clientCLIOptions.addOption(optionUpdate);

		OptionBuilder.withDescription(
				"Check for available updates for ClustEval components");
		optionUpdate = OptionBuilder.create("checkForComponentUpdates");
		clientCLIOptions.addOption(optionUpdate);

		// init valid command line options
		OptionBuilder.withArgName("ip");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"The ip address of the backend server to connect to.");
		option = OptionBuilder.create("ip");
		clientCLIOptions.addOption(option);

		OptionBuilder.withArgName("port");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"The port number of the backend server to connect to.");
		option = OptionBuilder.create("port");
		clientCLIOptions.addOption(option);

		OptionBuilder.withArgName("id");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"The client id for identification purposes of this client with the server.");
		option = OptionBuilder.create("clientId");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available datasets from the server.");
		option = OptionBuilder.create("getDataSets");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available programs from the server.");
		option = OptionBuilder.create("getPrograms");
		clientCLIOptions.addOption(option);

		OptionBuilder
				.withDescription("Queries the available runs from the server.");
		option = OptionBuilder.create("getRuns");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available run resumes from the server.");
		option = OptionBuilder.create("getRunResumes");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available run results from the server.");
		option = OptionBuilder.create("getRunResults");
		clientCLIOptions.addOption(option);

		OptionBuilder.withArgName("runName");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription("Queries the status of a certain run");
		option = OptionBuilder.create("getRunStatus");
		clientCLIOptions.addOption(option);

		OptionBuilder.withArgName("runName");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"Queries the exceptions thrown during execution of a certain run");
		option = OptionBuilder.create("getRunExceptions");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the exceptions thrown during parsing and loading the repository objects");
		option = OptionBuilder.create("getParsingExceptions");
		clientCLIOptions.addOption(option);

		OptionBuilder.withArgName("runName");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"Queries the estimated remaining runtime of a certain run");
		option = OptionBuilder.create("getEstimatedRemainingRunTime");
		clientCLIOptions.addOption(option);

		OptionBuilder.withArgName("runName");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"Queries the optimization status of a certain run");
		option = OptionBuilder.create("getOptRunStatus");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Gets the enqueued runs and run resumes of the backend server");
		option = OptionBuilder.create("getQueue");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Gets the running runs and run resumes of the backend server");
		option = OptionBuilder.create("getRunningRuns");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Gets the finished runs and run resumes of the backend server");
		option = OptionBuilder.create("getFinishedRuns");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Gets the terminated runs and run resumes of the backend server");
		option = OptionBuilder.create("getTerminatedRuns");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Gets the currently active threads and the corresponding iterations which they perform");
		option = OptionBuilder.create("getActiveThreads");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription("Gets the queued iteration runnables");
		option = OptionBuilder.create("getIterationRunnableQueue");
		clientCLIOptions.addOption(option);

		OptionBuilder.withArgName("threadNumber");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"Sets the maximal number of parallel threads.");
		option = OptionBuilder.create("setThreadNumber");
		clientCLIOptions.addOption(option);

		OptionBuilder.withArgName("runName");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"Performs a certain run (if not already running)");
		option = OptionBuilder.create("performRun");
		clientCLIOptions.addOption(option);

		OptionBuilder.withArgName("runName");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"Resumes a certain run that was started and terminated earlier.");
		option = OptionBuilder.create("resumeRun");
		clientCLIOptions.addOption(option);

		OptionBuilder.withArgName("runName");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"Terminates a certain run that was started earlier.");
		option = OptionBuilder.create("terminateRun");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription("Shut down ClustEval.");
		option = OptionBuilder.create("shutdown");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"The client will wait until all runs that this client started are finished.");
		option = OptionBuilder.create("waitForRuns");
		clientCLIOptions.addOption(option);

		OptionBuilder.withArgName("generatorName");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription("Generates a dataset using the generator "
				+ "with the given name. Parameters for "
				+ "this generator can be specified after "
				+ "this command has been executed.");
		option = OptionBuilder.create("generateDataSet");
		clientCLIOptions.addOption(option);

		OptionBuilder.withArgName("randomizerName");
		OptionBuilder.hasArg();
		OptionBuilder
				.withDescription("Randomizes a dataconfig using the randomizer "
						+ "with the given name. Parameters for "
						+ "this randomizer can be specified after "
						+ "this command has been executed.");
		option = OptionBuilder.create("randomizeDataConfig");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available program configurations from the server.");
		option = OptionBuilder.create("getProgramConfigurations");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available data configurations from the server.");
		option = OptionBuilder.create("getDataConfigurations");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available data set configurations from the server.");
		option = OptionBuilder.create("getDataSetConfigurations");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available gold standards from the server.");
		option = OptionBuilder.create("getGoldStandards");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available gold standard configurations from the server.");
		option = OptionBuilder.create("getGoldStandardConfigurations");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available clustering quality measures from the server.");
		option = OptionBuilder.create("getClusteringQualityMeasures");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available parameter optimization methods from the server.");
		option = OptionBuilder.create("getParameterOptimizationMethods");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available data statistics from the server.");
		option = OptionBuilder.create("getDataStatistics");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available run statistics from the server.");
		option = OptionBuilder.create("getRunStatistics");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available run-data statistics from the server.");
		option = OptionBuilder.create("getRunDataStatistics");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available data preprocessors from the server.");
		option = OptionBuilder.create("getDataPreprocessors");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available distance measures from the server.");
		option = OptionBuilder.create("getDistanceMeasures");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available input data set formats from the server.");
		option = OptionBuilder.create("getDataSetFormats");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available run result formats from the server.");
		option = OptionBuilder.create("getRunResultFormats");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the available data set types from the server.");
		option = OptionBuilder.create("getDataSetTypes");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the absolute path of the repository.");
		option = OptionBuilder.create("getAbsoluteRepositoryPath");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Queries the aliases of available programs from the server.");
		option = OptionBuilder.create("getProgramAliases");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Debug parameter: Do not calculate any new clusterings");
		option = OptionBuilder.create("DnoClusterings");
		clientCLIOptions.addOption(option);

		OptionBuilder.withArgName("programConfigName");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"Queries the parameters of a specified program configuration from the server.");
		option = OptionBuilder.create("getProgramConfigParams");
		clientCLIOptions.addOption(option);

		// OptionBuilder.withArgName("host:port");
		// OptionBuilder.hasArgs(2);
		// OptionBuilder.withValueSeparator(':');
		// OptionBuilder.withDescription("TODO...");
		// option = OptionBuilder.create("addBackendServerSlave");
		// clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Triggers an update of the materialized view of the database.");
		option = OptionBuilder.create("refreshDatabaseMaterializedViews");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription(
				"Removes all run results from the database and reparses them.");
		option = OptionBuilder.create("reparseAllRunResultsIntoDatabase");
		clientCLIOptions.addOption(option);

		OptionBuilder.withArgName("runResultId");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription(
				"Removes the run result corresponding to the passed ID from the database and reparses it.");
		option = OptionBuilder.create("reparseRunResultIntoDatabase");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription("Get your permissions.");
		option = OptionBuilder.create("getPermissions");
		clientCLIOptions.addOption(option);

		OptionBuilder.withDescription("Print your Client ID.");
		option = OptionBuilder.create("printClientId");
		clientCLIOptions.addOption(option);

		OptionBuilder
				.withDescription("Retrieve runs that terminated with errors.");
		option = OptionBuilder.create("getErrorRuns");
		clientCLIOptions.addOption(option);

	}

	/**
	 * A wrapper object holding the parsed command line parameters of this
	 * client. There are some connection parameters:
	 * <ul>
	 * <li><b>ip</b>: The ip address of the server</li>
	 * <li><b>port</b>: The port of the server</li>
	 * <li><b>clientId</b>: The id this client should use</li>
	 * </ul>
	 * There are other command line parameters, that cause the client to
	 * terminate immediately after the command has been executed.
	 * <ul>
	 * <li><b>getDataSets</b>: This tells the client to get and print the
	 * available datasets of the server.</li>
	 * <li><b>getPrograms</b>: This tells the client to get and print the
	 * available programs of the server.</li>
	 * <li><b>getRuns</b>: This tells the client to get and print the available
	 * runs of the server.</li>
	 * <li><b>getRunResumes</b>: This tells the client to get and print all the
	 * run result directories contained in the repository of this server. Those
	 * run result directories can be resumed, if they were terminated before.
	 * </li>
	 * <li><b>getRunResults</b>: This tells the client to get and print all the
	 * run result directories contained in the repository of this server, that
	 * contain a clusters subfolder and at least one *.complete file containing
	 * results (can be slow if many run result folders are present).</li>
	 * <li><b>getRunStatus</b>: This tells the client to get the status and
	 * percentage (if) of a certain run.</li>
	 * <li><b>performRun XXXX</b>: This tells the client to perform a run with a
	 * certain name.</li>
	 * <li><b>resumeRun XXXX</b>: This tells the client to resume a run
	 * previously performed identified by its run result identifier.</li>
	 * <li><b>terminateRun XXXX</b>: This tells the client to terminate the
	 * execution of a run with a certain name.</li>
	 * <li><b>shutdown</b>: This tells the client to shutdown the framework.
	 * </li>
	 * <li><b>waitForRuns</b>: This option can be used together with
	 * getRunStatus, in order to cause the client to wait until the run has
	 * finished its execution.</li>
	 * </ul>
	 */
	private CommandLine params;

	/**
	 * This attribute holds only those arguments not yet parsed. Those can be
	 * passed on to another commands like dataset generators.
	 */
	private String[] args;

	/**
	 * Instantiates a new eval client.
	 * 
	 * <p>
	 * If no port is specified in the options, the default 1099 will be used.
	 * 
	 * <p>
	 * If no ip is specified, the localhost will be used.
	 * 
	 * <p>
	 * If no clientId is specified, the client will retrieve a new one from the
	 * server.
	 * 
	 * @param params
	 *            The command line parameters for this client (see
	 *            {@link #params}).
	 * 
	 * @throws ConnectException
	 * @throws ParseException
	 * @throws IncompatibleClustEvalVersionException
	 * @throws InterruptedException
	 */
	public ClustEvalCommandLineClient(final String[] params)
			throws ConnectException, UnknownHostException, ParseException,
			IncompatibleClustEvalVersionException {
		super();

		try {
			this.params = parseParams(params, true);
		} catch (ParseException e) {

			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("ClustEvalBackendClient.jar", "",
					clientCLIOptions, "Available commands are:", false);

			throw e;
		}

		/**
		 * Keep a list of not parsed arguments. This has to be done, to maintain
		 * the - of options passed, since they are not kept in the
		 * CommandLine#args attribute.
		 */
		List<String> notParsedArgs = new ArrayList<String>();
		for (String s : params) {
			String raw;
			if (s.startsWith("-"))
				raw = s.substring(1);
			else
				raw = s;

			if (this.params.getArgList().contains(raw)
					|| this.params.getArgList().contains(s))
				notParsedArgs.add(s);
		}
		this.args = notParsedArgs.toArray(new String[0]);

		if (this.params.hasOption("clientId"))
			this.clientId = this.params.getOptionValue("clientId");
		if (this.params.hasOption("port"))
			this.port = Integer.parseInt(this.params.getOptionValue("port"));
		else
			// default port
			this.port = 1099;
		if (this.params.hasOption("ip"))
			this.ip = this.params.getOptionValue("ip");

		connectToServer();

		thread = new Thread(this);
		thread.start();
		try {
			thread.join();
		} catch (InterruptedException e) {
		}
	}

	public final synchronized void join() throws InterruptedException {
		this.thread.join();
	}

	/**
	 * A helper method for {@link #main(String[])} to parse the command line
	 * parameters and put them into a wrapper object.
	 * 
	 * @param params
	 *            The input command line parameters.
	 * @param stopAtNonOptions
	 *            A boolean indicating, whether to throw an exception on unknown
	 *            options.
	 * @return A wrapper object containing the parsed and valid parameters.
	 * @throws ParseException
	 */
	private static CommandLine parseParams(String[] params,
			boolean stopAtNonOptions) throws ParseException {

		CommandLineParser parser = new PosixParser();
		CommandLine cmd = parser.parse(clientCLIOptions, params,
				stopAtNonOptions);

		Level logLevel;
		if (cmd.hasOption("logLevel")) {
			switch (Integer.parseInt(cmd.getOptionValue("logLevel"))) {
				case 0 :
					logLevel = Level.ALL;
					break;
				case 1 :
					logLevel = Level.TRACE;
					break;
				case 2 :
					logLevel = Level.DEBUG;
					break;
				case 3 :
					logLevel = Level.INFO;
					break;
				case 4 :
					logLevel = Level.WARN;
					break;
				case 5 :
					logLevel = Level.ERROR;
					break;
				case 6 :
					logLevel = Level.OFF;
					break;
				default :
					throw new ParseException(
							"The logLevel argument requires one of the value of [0,1,2,3,4,5,6]");
			}
		} else {
			logLevel = Level.INFO;
		}
		((ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME)).setLevel(logLevel);

		return cmd;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		boolean checkForRunStatus = false;
		boolean checkForOptRunStatus = false;
		try {
			try {
				if (params.hasOption("performRun")) {
					try {
						System.out.println(this.performRun(
								params.getOptionValue("performRun")));
					} catch (ObjectNotRegisteredException e) {
						System.out.println("Unknown run");
					} catch (ObjectVersionNotRegisteredException e) {
						System.out.println("Unknown run version");
					}
				}
				if (params.hasOption("resumeRun")) {
					this.resumeRun(params.getOptionValue("resumeRun"));
				}
				if (params.hasOption("terminateRun")) {
					this.terminateRun(params.getOptionValue("terminateRun"));
				}
				if (params.hasOption("getRuns")) {
					System.out.println("Runs: " + this.getRuns());
				}
				if (params.hasOption("getRunResumes")) {
					System.out.println("RunResumes: " + this.getRunResumes());
				}
				if (params.hasOption("getQueue")) {
					System.out.println("Queue: " + this.getQueue());
				}
				if (params.hasOption("getRunningRuns")) {
					System.out
							.println("Running runs: " + this.getRunningRuns());
				}
				if (params.hasOption("getTerminatedRuns")) {
					System.out.println(
							"Terminated runs: " + this.getTerminatedRuns());
				}
				if (params.hasOption("getFinishedRuns")) {
					System.out.println(
							"Finished runs: " + this.getFinishedRuns());
				}
				if (params.hasOption("getActiveThreads")) {
					printActiveThreads();
				}
				if (params.hasOption("help")) {
					HelpFormatter formatter = new HelpFormatter();
					formatter.printHelp("clustevalBackendClient.jar",
							clientCLIOptions);
				}
				if (params.hasOption("version")) {
					System.out.println(VERSION_INFO_STRING);
					System.out.println("=========================");
				}
				if (params.hasOption("checkForServerUpdate")) {
					ComparableVersion checkForUpdate = this
							.checkForServerUpdate(true);
					if (checkForUpdate != null)
						System.out.println(
								"An updated version of the ClustEval server is available: "
										+ checkForUpdate);
					else
						System.out.println(
								"This version of the ClustEval server is up-to-date");
				}
				if (params.hasOption("checkForClientUpdate")) {
					ComparableVersion checkForUpdate = this
							.checkForUpdate(true);
					if (checkForUpdate != null)
						System.out.println(
								"An updated version of the ClustEval client is available: "
										+ checkForUpdate);
					else
						System.out.println(
								"This version of the ClustEval client is up-to-date");
				}
				if (params.hasOption("checkForComponentUpdates")) {
					Map<String, Map<String, String>> checkForComponentUpdates = new TreeMap<>(
							this.checkForComponentUpdates(true));
					if (!checkForComponentUpdates.isEmpty()) {
						System.out.println(
								"The following components are outdated:");

						int[] columnWidths = new int[]{80, 10, 10};
						String formatStringHeader = String.format(
								"\t%%-%ds %%-%ds %%-%ds\n", columnWidths[0],
								columnWidths[1], columnWidths[2]);
						String formatStringWithColors = String.format(
								"\t%%-%ds \033[0;31m%%-%ds\033[0m \033[0;32m%%-%ds\033[0m\n",
								columnWidths[0], columnWidths[1],
								columnWidths[2]);
						System.out.print(String.format(formatStringHeader,
								"Component", "Version", "New version"));

						for (String className : checkForComponentUpdates
								.keySet()) {
							for (String usedVersion : checkForComponentUpdates
									.get(className).keySet()) {

								System.out.print(String.format(
										formatStringWithColors,
										StringExt.ellipsis(className,
												columnWidths[0],
												ELLIPSIS_LOCATION.STRING_START),
										StringExt.ellipsis(usedVersion,
												columnWidths[1],
												ELLIPSIS_LOCATION.STRING_START),
										StringExt.ellipsis(
												checkForComponentUpdates
														.get(className)
														.get(usedVersion),
												columnWidths[2],
												ELLIPSIS_LOCATION.STRING_START)));
							}
						}
					}
				}
				if (params.hasOption("getIterationRunnableQueue")) {
					printIterationRunnableQueue();
				}
				if (params.hasOption("setThreadNumber")) {
					this.server.setThreadNumber(clientId, Integer
							.valueOf(params.getOptionValue("setThreadNumber")));
				}
				if (params.hasOption("DnoClusterings")) {
					this.server.setDebugNoNewClusterings(clientId, true);
				}
				if (params.hasOption("getPermissions")) {
					IClientPermissions perms = this.getClientPermissions();
					System.out.println("Permissions:");
					System.out.println(String.format("Shutdown server:\t%s",
							Boolean.toString(perms.canShutdown())));
					System.out.println(String.format("Start run:\t%s",
							Boolean.toString(perms.canStartRun())));
					System.out.println(String.format("Stop run:\t%s",
							Boolean.toString(perms.canStopRun())));
				}
				if (params.hasOption("printClientId")) {
					printClientID(clientId);
				}
				if (params.hasOption("getRunResults")) {
					Map<Pair<String, String>, Map<String, Double>> result = this
							.getRunResults(
									params.getOptionValue("getRunResults"));
					boolean first = true;
					for (Pair<String, String> p : result.keySet()) {
						if (first) {
							for (String m : result.get(p).keySet()) {
								System.out.format("%-30s", "");
								System.out.print("\t" + m);
							}
							System.out.println();
						}
						System.out.format("%-30s",
								"(" + p.getFirst() + "," + p.getSecond() + ")");
						for (String m : result.get(p).keySet()) {
							System.out.println("\t" + result.get(p).get(m));
						}
						first = false;
					}
				}
				if (params.hasOption("getDataSets")) {
					System.out.println("DataSets: " + this.getDataSets());
				}
				if (params.hasOption("getPrograms")) {
					System.out.println("Programs: " + this.getPrograms());
				}
				if (params.hasOption("getProgramConfigurations")) {
					System.out.println("Program configurations: "
							+ this.getProgramConfigurations());
				}
				if (params.hasOption("getDataConfigurations")) {
					System.out.println("Data configurations: "
							+ this.getDataConfigurations());
				}
				if (params.hasOption("getDataSetConfigurations")) {
					System.out.println("Data set configurations: "
							+ this.getDataSetConfigurations());
				}
				if (params.hasOption("getGoldStandards")) {
					System.out.println(
							"Gold standards: " + this.getGoldStandards());
				}
				if (params.hasOption("getGoldStandardConfigurations")) {
					System.out.println("Gold standard configurations: "
							+ this.getGoldStandardConfigurations());
				}
				if (params.hasOption("getClusteringQualityMeasures")) {
					System.out.println("Clustering quality measures: "
							+ this.getClusteringQualityMeasures());
				}
				if (params.hasOption("getParameterOptimizationMethods")) {
					System.out.println("Parameter optimization methods: "
							+ this.getParameterOptimizationMethods());
				}
				if (params.hasOption("getDataStatistics")) {
					System.out.println(
							"Data statistics: " + this.getDataStatistics());
				}
				if (params.hasOption("getRunStatistics")) {
					System.out.println(
							"Run statistics: " + this.getRunStatistics());
				}
				if (params.hasOption("getRunDataStatistics")) {
					System.out.println("Run data statistics: "
							+ this.getRunDataStatistics());
				}
				if (params.hasOption("getDataPreprocessors")) {
					System.out.println("Data preprocessors: "
							+ this.getDataPreprocessors());
				}
				if (params.hasOption("getDistanceMeasures")) {
					System.out.println(
							"Distance measures: " + this.getDistanceMeasures());
				}
				if (params.hasOption("getDataSetFormats")) {
					System.out.println(
							"Data set formats: " + this.getDataSetFormats());
				}
				if (params.hasOption("getRunResultFormats")) {
					System.out.println("Run result formats: "
							+ this.getRunResultFormats());
				}
				if (params.hasOption("getDataSetTypes")) {
					System.out.println(
							"Data set types: " + this.getDataSetTypes());
				}
				if (params.hasOption("getAbsoluteRepositoryPath")) {
					System.out.println("Absolute repository path: "
							+ this.getAbsoluteRepositoryPath());

				}
				if (params.hasOption("getRunExceptions")) {
					for (String s : this.getRunExceptions(
							params.getOptionValue("getRunExceptions"), false))
						System.out.println(s);

				}
				if (params.hasOption("getParsingExceptions")) {
					System.out.println(this.getParsingExceptionMessages());
				}
				if (params.hasOption("getRunStatus")) {
					checkForRunStatus = true;
				}
				if (params.hasOption("getOptRunStatus")) {
					checkForOptRunStatus = true;
				}
				if (params.hasOption("shutdown")) {
					this.shutdownFramework();
				}
				if (params.hasOption("generateDataSet")) {
					this.generateDataSet();
				}
				if (params.hasOption("getProgramConfigParams")) {
					try {
						System.out.println(
								this.getParametersForProgramConfiguration(
										params.getOptionValue(
												"getProgramConfigParams")));
					} catch (ObjectNotRegisteredException e) {
						System.out.println("Unknown program configuration");
					} catch (ObjectVersionNotRegisteredException e) {
						System.out.println(
								"Unknown program configuration version");
					}
				}
				if (params.hasOption("getProgramAliases")) {
					System.out.println(this.getProgramAliases());
				}
				// if (params.hasOption("addBackendServerSlave")) {
				// this.addBackendServerSlave(params.getOptionValues("addBackendServerSlave"));
				// }
				if (params.hasOption("refreshDatabaseMaterializedViews")) {
					this.server.refreshDatabaseMaterializedViews(clientId);
				}
				if (params.hasOption("reparseAllRunResultsIntoDatabase")) {
					this.server.reparseAllRunResultsIntoDatabase(clientId);
				}
				if (params.hasOption("reparseRunResultIntoDatabase")) {
					this.server.reparseRunResultIntoDatabase(clientId, params
							.getOptionValue("reparseRunResultIntoDatabase"));
				}
				if (params.hasOption("getErrorRuns")) {
					try {
						System.out.println(this.getErrorRuns());
					} catch (ObjectNotFoundException e) {
						e.printStackTrace();
					} catch (ObjectVersionNotFoundException e) {
						e.printStackTrace();
					}
				}
				if (params.hasOption("randomizeDataConfig")) {
					String randomizerName = params
							.getOptionValue("randomizeDataConfig");

					CommandLineParser parser = new PosixParser();
					Options options = getOptionsForDataRandomizer(
							randomizerName);

					try {
						parser.parse(options, this.args);
						this.server.randomizeDataConfig(clientId,
								randomizerName, this.args);
					} catch (ParseException e1) {
						try {
							reader.println();
							HelpFormatter formatter = new HelpFormatter();
							formatter.setOptionComparator(
									new MyOptionComparator());
							formatter.printHelp(
									"randomizeDataConfig " + randomizerName,
									options, true);
							reader.println();
						} catch (IOException e) {
							e.printStackTrace();
						}
					} catch (DataRandomizeException e) {
						e.printStackTrace();
					}
				}

				if (checkForRunStatus) {
					try {
						String runName = params.getOptionValue("getRunStatus");

						Map<String, Pair<RUN_STATUS, Float>> status = null;
						try {
							do {
								if ((status = this.getMyRunStatus()) != null
										&& status.size() > 0) {
									RUN_STATUS newStatus;
									Float percent;
									if (!status.containsKey(runName)) {
										log.info("No run with name " + runName
												+ " running.");
										return;
									}
									newStatus = status.get(runName).getFirst();

									percent = status.get(runName).getSecond();
									String printString = String.format(
											"\r%.2f%% (%s)", percent,
											WordUtils.capitalizeFully(
													newStatus.toString()));
									System.out.print(printString);
									if (newStatus == RUN_STATUS.FINISHED)
										break;
									Thread.sleep(100);
								}
							} while (params.hasOption("waitForRuns"));
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						System.out.println();
					} catch (UnknownHostException e2) {
						this.log.warn(
								"The server terminated the connection...");
					} catch (ConnectException e2) {
						this.log.warn(
								"The server terminated the connection...");
					} catch (RemoteException e2) {
						e2.printStackTrace();
					}
				}

				if (params.hasOption("getEstimatedRemainingRunTime")) {
					String runName = params
							.getOptionValue("getEstimatedRemainingRunTime");

					try {
						do {
							Map<String, Long> etas = getMyEstimatedRuntime();

							if (!etas.containsKey(runName)) {
								log.info("No run with name " + runName
										+ " running.");
								return;
							}
							if (this.getMyRunStatus().get(runName)
									.getFirst() == RUN_STATUS.FINISHED)
								break;
							System.out
									.print("\r" + Formatter.formatMsToDuration(
											etas.get(runName) * 1000));
							Thread.sleep(1000);
						} while (params.hasOption("waitForRuns"));
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println();
				}
				if (checkForOptRunStatus) {
					try {

						String runName = params
								.getOptionValue("getOptRunStatus");

						// runId ->
						// ((Status,%),(ProgramConfig,DataConfig)->(QualityMeasure->(ParameterSet->Quality)))
						Map<String, Pair<Pair<RUN_STATUS, Float>, Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>>>> optStatus = null;
						if ((optStatus = this
								.getMyOptimizationRunStatus()) != null
								&& optStatus.size() > 0) {
							RUN_STATUS newStatus;
							Float percent;

							if (!optStatus.containsKey(runName)) {
								log.info("No run with name " + runName
										+ " running.");
								return;
							}
							newStatus = optStatus.get(runName).getFirst()
									.getFirst();
							percent = optStatus.get(runName).getFirst()
									.getSecond();
							System.out.println();
							System.out.println("\r Status:\t" + newStatus + " "
									+ String.format("%.3f", percent) + "%");
							Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>> qualities = optStatus
									.get(runName).getSecond();

							// print the quality measures; just take them from
							// the
							// first pair of programConfig and dataConfig
							// (runnable)
							Set<String> qualityMeasuresSet = new HashSet<String>();
							Iterator<Pair<Double, Map<String, Pair<Map<String, String>, String>>>> runPairIt = qualities
									.values().iterator();
							while (runPairIt.hasNext()) {
								Pair<Double, Map<String, Pair<Map<String, String>, String>>> runPair = runPairIt
										.next();
								qualityMeasuresSet
										.addAll(runPair.getSecond().keySet());
							}
							String[] qualityMeasures = qualityMeasuresSet
									.toArray(new String[0]);
							Arrays.sort(qualityMeasures);
							int pos = 0;
							while (true) {
								boolean foundMeasure = false;
								for (String measure : qualityMeasures) {
									if (pos < measure.length()) {
										System.out.printf("\t%s",
												measure.charAt(pos));
										foundMeasure = true;
									} else
										System.out.print("\t");
								}
								System.out.println();
								if (!foundMeasure)
									break;
								pos++;
							}

							// 06.06.2014: added sets to keep order when
							// printing
							// the results
							Set<String> programConfigs = new HashSet<String>();
							Set<String> dataConfigs = new HashSet<String>();

							for (Pair<String, String> pcDcPair : qualities
									.keySet()) {
								programConfigs.add(pcDcPair.getFirst());
								dataConfigs.add(pcDcPair.getSecond());
							}

							for (String programConfig : programConfigs) {
								System.out.printf("%s:\n", programConfig);
								for (String dataConfig : dataConfigs) {
									Pair<String, String> pcDcPair = Pair
											.getPair(programConfig, dataConfig);
									if (!qualities.containsKey(pcDcPair)) {
										System.out.printf(
												"-- %s:\t\t\tStatus:\t--%%\n",
												dataConfig);
										continue;
									}
									Map<String, Pair<Map<String, String>, String>> qualitiesPcDc = qualities
											.get(pcDcPair).getSecond();
									System.out.printf(
											"-- %s:\t\t\tStatus:\t%.1f%%\n",
											dataConfig,
											qualities.get(pcDcPair).getFirst());
									for (String measure : qualityMeasures) {
										if (!qualitiesPcDc
												.containsKey(measure)) {
											System.out.print("\t");
											continue;
										}
										String quality = qualitiesPcDc
												.get(measure).getSecond();
										if (quality.equals("NT"))
											System.out.print("\tNT");
										else {
											double qualityDouble = Double
													.valueOf(quality);
											if (Double
													.isInfinite(qualityDouble))
												System.out.printf("\t%s%s",
														qualityDouble < 0
																? "-"
																: "",
														"Inf");
											else
												System.out.printf("\t%.4f",
														qualityDouble);
										}
									}
									System.out.println();
								}
							}
						}
						System.out.println();
					} catch (UnknownHostException e2) {
						this.log.warn(
								"The server terminated the connection...");
					} catch (ConnectException e2) {
						this.log.warn(
								"The server terminated the connection...");
					} catch (RemoteException e2) {
						e2.printStackTrace();
					}
				}
			} catch (UnknownClientException e) {
				System.out.println(
						"Your client id is invalid. Possibly, the server has been restarted in the meantime.");
				reaquireUserId();
			} catch (OperationNotPermittedException e) {
				System.out.println("Operation not permitted");
			} catch (UnknownDataRandomizerException e2) {
				e2.printStackTrace();
			} catch (DynamicComponentInitializationException e2) {
				e2.printStackTrace();
			} catch (UnknownDataSetGeneratorException e) {
				e.printStackTrace();
			} catch (UnknownRProgramException e) {
				e.printStackTrace();
			} catch (RepositoryObjectSerializationException e) {
				e.printStackTrace();
			} catch (UnknownDataSetFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnknownRunResultFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidDynamicComponentNameException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (NoSuchObjectException e) {
			this.log.warn(
					"The connection to the server has been lost. Trying to reconnect ...");
			try {
				connectToServer();
			} catch (UnknownHostException e1) {
				this.log.warn("failed: " + e1.getMessage());
			} catch (ConnectException e1) {
				this.log.warn("failed: " + e1.getMessage());
			} catch (IncompatibleClustEvalVersionException e1) {
				this.log.warn("failed: " + e1.getMessage());
				System.exit(0);
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @throws RemoteException
	 * @throws DynamicComponentInitializationException
	 * @throws UnknownDataSetGeneratorException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 * 
	 */
	private void generateDataSet()
			throws RemoteException, UnknownDataSetGeneratorException,
			DynamicComponentInitializationException,
			OperationNotPermittedException, UnknownClientException {
		String generatorName = params.getOptionValue("generateDataSet");

		CommandLineParser parser = new PosixParser();
		Options options = getOptionsForDataSetGenerator(generatorName);

		try {
			List<String[]> parameterSets = splitParameterValues(options,
					this.args);

			for (String[] vals : parameterSets) {
				parser.parse(options, vals);
				this.server.generateDataSet(clientId, generatorName, vals);
			}
		} catch (ParseException e1) {
			try {
				reader.println();
				HelpFormatter formatter = new HelpFormatter();
				formatter.setOptionComparator(new MyOptionComparator());
				formatter.printHelp("generateDataSet " + generatorName, options,
						true);
				reader.println();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * This method takes the original parameters, where one parameter value can
	 * contain semicolon separated values. It then creates one parameter set for
	 * each possible combination of parameter value. <br>
	 * We assume parameter values to be separated by semicolon. And we assume
	 * parameter names in the fileName and alias to contain placeholders of the
	 * form %paramName%. <br>
	 * The fileName and alias parameters have to contain all parameter names,
	 * such that the resulting names are unique.
	 * 
	 * @param args2
	 * @return
	 * @throws ParseException
	 */
	private List<String[]> splitParameterValues(final Options options,
			final String[] args2) throws ParseException {
		Map<String, String[]> splitValues = new HashMap<String, String[]>();

		Set<String> paramsRequiredInName = new HashSet<String>();

		String currParam = null;

		for (int i = 0; i < args2.length; i++) {
			String origVal = args2[i];

			if (origVal.startsWith("-")) {
				// if it is a switch parameter, we can denote it with a ; at the
				// end to indicate,
				// that we want to treat it as present and non present.
				String[] s = origVal.substring(1).split(";");

				currParam = s[0];

				// switch parameter
				if (!(options.getOption(currParam).hasArg())) {
					if (origVal.contains(";")) {
						splitValues.put(currParam,
								new String[]{"true", "false"});
						paramsRequiredInName.add(currParam);
					} else
						splitValues.put(currParam, new String[]{"true"});
				}
			} else {
				String[] valSplit = origVal.split(";");
				splitValues.put(currParam, valSplit);
				if (valSplit.length > 1)
					paramsRequiredInName.add(currParam);
			}
		}

		// check, that the fileName and alias parameter contain all
		// parameters with more than 1 value;
		if (!splitValues.containsKey("fileName")
				|| !splitValues.containsKey("alias"))
			throw new ParseException("");

		String fileName = splitValues.get("fileName")[0];
		String alias = splitValues.get("alias")[0];

		for (String requiredParamInName : paramsRequiredInName) {
			if (!fileName
					.contains(String.format("%%%s%%", requiredParamInName)))
				throw new IllegalArgumentException(String.format(
						"The fileName has to contain a placeholder for the %s parameter, because it has more than 1 value.",
						requiredParamInName));
			if (!alias.contains(String.format("%%%s%%", requiredParamInName)))
				throw new IllegalArgumentException(String.format(
						"The alias has to contain a placeholder for the %s parameter, because it has more than 1 value.",
						requiredParamInName));
		}

		List<String[]> result = new ArrayList<String[]>();
		// storing a mapping from parameter to replacement value; to replace the
		// parameter placeholders in fileName and alias
		List<Map<String, String>> paramValueMaps = new ArrayList<Map<String, String>>();
		result.add(new String[0]);
		paramValueMaps.add(new HashMap<String, String>());

		for (String paramName : splitValues.keySet()) {
			if (paramName == null || paramName.equals("fileName")
					|| paramName.equals("alias"))
				continue;
			boolean isSwitch = !options.getOption(paramName).hasArg();
			List<String[]> newParamSets = new ArrayList<String[]>();
			List<Map<String, String>> newParamValueMaps = new ArrayList<Map<String, String>>();
			for (int i = 0; i < result.size(); i++) {
				String[] incompleteParams = result.get(i);
				Map<String, String> incompleteParamMap = paramValueMaps.get(i);
				for (String paramValue : splitValues.get(paramName)) {

					// switch parameter
					if (isSwitch) {
						if (paramValue.equals("true")) {
							newParamSets.add(ArraysExt.merge(incompleteParams,
									new String[]{"-" + paramName}));
							Map<String, String> newParamMap = new HashMap<String, String>(
									incompleteParamMap);
							newParamMap.put(paramName, paramName + "_true");
							newParamValueMaps.add(newParamMap);
						} else {
							newParamSets.add(incompleteParams);
							Map<String, String> newParamMap = new HashMap<String, String>(
									incompleteParamMap);
							newParamMap.put(paramName, paramName + "_false");
							newParamValueMaps.add(newParamMap);
						}
					} else {
						newParamSets.add(ArraysExt.merge(incompleteParams,
								new String[]{"-" + paramName, paramValue}));
						Map<String, String> newParamMap = new HashMap<String, String>(
								incompleteParamMap);
						newParamMap.put(paramName, paramValue);
						newParamValueMaps.add(newParamMap);
					}
				}
			}
			result = newParamSets;
			paramValueMaps = newParamValueMaps;
		}

		// replace placeholders in fileName and alias
		for (int i = 0; i < paramValueMaps.size(); i++) {
			Map<String, String> paramValueMap = paramValueMaps.get(i);
			String newFileName = fileName;
			String newAlias = alias;
			for (String paramName : paramValueMap.keySet()) {
				newFileName = newFileName.replace(
						String.format("%%%s%%", paramName),
						paramValueMap.get(paramName));
				newAlias = newAlias.replace(String.format("%%%s%%", paramName),
						paramValueMap.get(paramName));
			}

			result.set(i, ArraysExt.merge(result.get(i), new String[]{
					"-fileName", newFileName, "-alias", newAlias}));
		}

		return result;
	}

	static ConsoleReader reader;

	private Thread thread;

	/**
	 * @param args
	 *            Command line parameters for the backend client (see
	 *            {@link #params}).
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
	public static void main(String[] args) throws IOException {
		try {
			CommandLine params = parseParams(args, false);

			if (params.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("clustevalClient", clientCLIOptions);
				System.exit(0);
			}

			if (params.hasOption("version")) {
				System.out.println(VERSION_INFO_STRING);
				System.exit(0);
			}

			initLogging(params);

			Logger log = LoggerFactory
					.getLogger(ClustEvalCommandLineClient.class);

			// if command line arguments (except connection parameters) are
			// passed, we do not start a console
			Set<String> paramKeys = new HashSet<String>();
			@SuppressWarnings("unchecked")
			Iterator<Option> it = params.iterator();
			while (it.hasNext()) {
				paramKeys.add(it.next().getOpt());
			}

			paramKeys.remove("ip");
			paramKeys.remove("port");
			paramKeys.remove("clientId");
			if (paramKeys.size() > 0) {
				try {
					new ClustEvalCommandLineClient(args);
				} catch (Exception e) {
				}
			} else {
				String clientId;
				if (params.hasOption("clientId"))
					clientId = params.getOptionValue("clientId");
				else
					// parse args because of possible connection parameters
					clientId = new ClustEvalCommandLineClient(args).clientId;

				printClientID(clientId);

				reader = new ConsoleReader();

				List<Completer> completers = new LinkedList<Completer>();
				completers.add(new BackendClientCompleter(clientId, args));

				String ip = params.hasOption("ip")
						? params.getOptionValue("ip")
						: "localhost";
				String port = params.hasOption("port")
						? params.getOptionValue("port")
						: "1099";

				setDefaultPromptAndCompleter(ip, port, completers);

				String line;

				while ((line = reader.readLine()) != null) {
					updatePrompt(ip, port);

					if (line.equalsIgnoreCase("quit")
							|| line.equalsIgnoreCase("exit")) {
						break;
					}

					boolean connectException = false;
					long waitingTime = 100;
					do {
						if (connectException) {
							waitingTime *= 2;
						}
						try {
							new ClustEvalCommandLineClient(ArraysExt.merge(args,
									("-clientId " + clientId + " -" + line)
											.split(" ")));
							connectException = false;
						} catch (UnknownHostException e) {
							connectException = true;
							log.info(String.format("retrying in %d seconds ...",
									(int) Math.ceil(waitingTime / 1000.0)));
						} catch (ConnectException e) {
							connectException = true;
							log.info(String.format("retrying in %d seconds ...",
									(int) Math.ceil(waitingTime / 1000.0)));
						} catch (IncompatibleClustEvalVersionException e) {
							System.exit(0);
						} catch (Exception e) {
							log.warn(e.getMessage());
						}
						Thread.sleep(waitingTime);
					} while (connectException);
					// }
				}
			}
		} catch (ParseException e) {
			System.err.println("Parsing failed.  Reason: " + e.getMessage());

			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("clustevalClient",
					"Invoking this client without any parameters will open a shell with tab-completion.",
					clientCLIOptions, "", true);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	protected static void updatePrompt(String ip, String port) {
		reader.setPrompt(String.format("ClustEval %s:%s %s> ", ip, port,
				new SimpleDateFormat("dd/MM/YYYY HH:mm:ss", Locale.getDefault())
						.format(new Date())));
	}

	protected static void setDefaultPromptAndCompleter(String ip, String port,
			Collection<Completer> newCompleters) {
		updatePrompt(ip, port);

		List<Completer> oldCompleters = new LinkedList<Completer>(
				reader.getCompleters());
		for (Completer c : oldCompleters)
			reader.removeCompleter(c);

		for (Completer c : newCompleters) {
			reader.addCompleter(c);
		}
	}

	/**
	 * This method is responsible for creating all the appender that are added
	 * to the logger.
	 * <p>
	 * Three appenders are created:
	 * <ul>
	 * <li><b>ConsoleAppender</b>: Writes the logging output to the standard out
	 * </li>
	 * <li><b>FileAppender</b>: Writes the logging output as formatter text to
	 * the file clustevalClient.log</li>
	 * <li><b>FileAppender</b>: Writes the logging output in lilith binary
	 * format to the file clustevalClient.lilith</li>
	 * </ul>
	 * 
	 * @param cmd
	 *            The command line parameters including possible options of
	 *            logging
	 * @throws ParseException
	 */
	private static void initLogging(CommandLine cmd) throws ParseException {
		Logger log = LoggerFactory.getLogger(ClustEvalCommandLineClient.class);

		Level logLevel;
		if (cmd.hasOption("logLevel")) {
			switch (Integer.parseInt(cmd.getOptionValue("logLevel"))) {
				case 0 :
					logLevel = Level.ALL;
					break;
				case 1 :
					logLevel = Level.TRACE;
					break;
				case 2 :
					logLevel = Level.DEBUG;
					break;
				case 3 :
					logLevel = Level.INFO;
					break;
				case 4 :
					logLevel = Level.WARN;
					break;
				case 5 :
					logLevel = Level.ERROR;
					break;
				case 6 :
					logLevel = Level.OFF;
					break;
				default :
					throw new ParseException(
							"The logLevel argument requires one of the value of [0,1,2,3,4,5,6]");
			}
		} else {
			logLevel = Level.INFO;
		}

		ClustEvalClient.initLogging(logLevel);
	}
}
